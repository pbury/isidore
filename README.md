# Isidore

Collectetout ce qu'Isidore connait comme publications concernant le CIREL.

Les résultats sont exportés dans deux fichiers :
- `uris.txt` qui contient la liste des publications du laboratoire 
- `collect.csv` qui contient les informations sur chaque publication, à savoir
    - uri
    - titre : le titre de la publication
    - auteurs : la liste des auteurs séparés par des `;`
    - types: le (ou les) type(s) de publication
    - sujets: le (ou les) sujet(s) dont parle la publication séparés par des `;`
    - date: la date de publication
    - url : l'url de la publication
