import urllib.request
import json
import math
import sys
from lxml import etree
import pickle
import os.path
import xlsxwriter
import re
from urllib.parse import urlparse
import pprint

filename = 'uris_profeor.txt'
query_part = 'CIREL+profeor&after=2013'
xls_filename = 'extract_isidore.xlsx'
download_details = False

uris = []

if not os.path.isfile(filename):
    query = "https://api.rechercheisidore.fr/resource/search?q=" + query_part + "&output=json"
    with urllib.request.urlopen(query) as data:
        res = json.loads(data.read().decode('utf8'))['response']
        nb_items = res['replies']['meta']['items']
        page_items = res['replies']['meta']['pageItems']
        nb_pages = math.ceil(int(nb_items) / int(page_items)) + 2
        for page in range(1, nb_pages):
            print(page)
            query = "https://api.rechercheisidore.fr/resource/search?q=" + query_part + "&output=json&page="+str(page)
            with urllib.request.urlopen(query) as details:
                res2 = json.loads(details.read().decode('utf8'))['response']
                for found in res2['replies']['content']['reply']:
                    uris.append(found['uri'])
    with open(filename, 'w') as fh:
        for uri in uris:
            fh.write(uri + '\n')
else:
    with open('uris.txt', 'r') as fh:
        for line in fh:
            uris.append(line.strip())

if download_details:
    row = 0
    col = 0
    workbook = xlsxwriter.Workbook(xls_filename)
    worksheet = workbook.add_worksheet()
    for title in ["uri", "titre", "auteurs", "openAire", "types", "sujets", "date", "domaine", "url"]:
        worksheet.write(row, col, title)
        col += 1
    for query in uris:
        row += 1
        print(row, query)
        worksheet.write(row, 0, 'http://api.rechercheisidore.fr/resource/content?uri=' + query)
        with urllib.request.urlopen('http://api.rechercheisidore.fr/resource/content?uri=' + query) as data:
            xml_data = data.read()
            tree = etree.fromstring(xml_data)
            for title in tree.xpath("title"):
                worksheet.write(row, 1, title.text)
            author_list = ""
            for authors in tree.xpath('enrichedCreators/creator'):
                author_list = author_list +  authors.get('normalizedAuthor').replace(',', '') + " ; "
            worksheet.write(row, 2, author_list )
            type_list = ""
            for types in tree.xpath('types/type'):
                if re.match(r"info:.*", types.text):
                    worksheet.write(row, 3, types.text)
                else:
                    type_list = type_list + types.text + " ; "
            worksheet.write(row, 4, type_list)
            subject_list = ""
            for subjects in tree.xpath('subjects/subject'):
                if subjects is not None and subjects.text is not None:
                    subject_list = subject_list + subjects.text + " ; "
            worksheet.write(row, 5, subject_list)
            for date in tree.xpath('date'):
                worksheet.write(row, 6, date.get('origin'))
            for url in tree.xpath('url'):
                parsed = urlparse(url.text)
                worksheet.write(row, 7, parsed.netloc)
                worksheet.write(row, 8, url.text)
    workbook.close()
